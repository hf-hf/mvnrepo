# mvnrepo

#### 介绍
mvnrepo


#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 在maven仓库中使用私服
由于我们的私服里只有我们要发布的jar，其他各种jar都没有，因此不要把这个私服作为全局的镜像，切记切记。
 
在pom文件中project元素的下一层增加如下配置：

```
 <repositories>
   <repository>
     <id>mvnrepo</id>
     <name>mvn repository</name>
     <url>https://gitee.com/hf-hf/mvnrepo/blob/master</url>
   </repository>
 </repositories>
 
 <dependency>
	<groupId>top.hunfan</groupId>
	<artifactId>kindle-helper</artifactId>
	<version>1.0-SNAPSHOT</version>
 </dependency>
 
 <dependency>
	<groupId>top.hunfan</groupId>
	<artifactId>hftools</artifactId>
	<version>1.0-SNAPSHOT</version>
 </dependency>
```